from person import Person

class Professor(Person):
  def __init__(self, first_name, last_name, age, subject_list):
    super().__init__(first_name, last_name, age)
    self.subject_list = subject_list
    
  def get_professor_subjects(self):
    return self.subject_list
  
  def add_subject(self, subject_name):
    self.subject_list.append(subject_name)
  
  def remove_subject(self, subject_name):
    self.subject_list.remove(subject_name)