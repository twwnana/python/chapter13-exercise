from person import Person

class Student(Person):
  def __init__(self, first_name, last_name, age, lectures_list):
    super().__init__(first_name, last_name, age)
    self.lectures_list = lectures_list
    
  def get_student_lectures(self):
    return self.lectures_list
  
  def add_lecture(self, lecture_name):
    self.lectures_list.append(lecture_name)
  
  def remove_lectures(self, lecture_name):
    self.lectures_list.remove(lecture_name)