class Lecture:
  def __init__(self, name, max_number_of_students, duration, list_of_processors_giving_this_lecture):
    self.name = name
    self.max_number_of_students = max_number_of_students
    self.duration = duration
    self.list_of_processors_giving_this_lecture = list_of_processors_giving_this_lecture
    
  def lecture_name_and_duration(self):
    return f"Lecture: {self.name}, duration: {self.duration} hour(s)."
  
  def add_professor(self, professorObj):
    self.list_of_processors_giving_this_lecture.update(professorObj)
  
  def remove_professor(self, professorObj):
    self.list_of_processors_giving_this_lecture.pop(professorObj)