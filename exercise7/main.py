from student import Student
from lecture import Lecture
from professor import Professor

lectures = [
    { "lecture": Lecture("The emperor Protects", 10, 1, 4) },
    { "lecture": Lecture("Squashingn Space Communist", 10, 1, 3) },
    { "lecture": Lecture("Xenos - How to Purge 101", 10, 1.5, 5) },
    { "lecture": Lecture("Xenos - How to Purge 102", 10, 3, 7) },
    { "lecture": Lecture("50 Shades of the Sister's touch", 10, 2.5, 5) },
    { "lecture": Lecture("Fighting the Orks - How not to do it 5th Edition", 10, 1, 1) },
    { "lecture": Lecture("Inter-species reproduction - Inquisition Guidlines 244th Edition", 10, .5, 1) }
  ]

studentObj = Student("Sheen Ismhael", "Lim", 32, [lectures[0], lectures[1], lectures[4]])

print("===Student Test===")
subjects = ""
for subject in studentObj.get_student_lectures():
  subjects += f"   {subject['lecture'].lecture_name_and_duration()}\n"
print(f"Student: {studentObj.full_name()} attends the following subjects:\n{subjects}")

print(f"Adding {lectures[3]['lecture'].lecture_name_and_duration()}")
studentObj.add_lecture(lectures[3])
subjects = ""
for subject in studentObj.get_student_lectures():
  subjects += f"   {subject['lecture'].lecture_name_and_duration()}\n"
print(f"Student: {studentObj.full_name()} attends the following subjects:\n{subjects}")

print(f"Deleting: {lectures[1]['lecture'].lecture_name_and_duration()}")
studentObj.remove_lectures(lectures[1])
subjects = ""
for subject in studentObj.get_student_lectures():
  subjects += f"   {subject['lecture'].lecture_name_and_duration()}\n"
print(f"Student: {studentObj.full_name()} attends the following subjects:\n{subjects}")

print("===Professor Test===")
professorObj1 = Professor("Gabriel", "Angelos", 423, [lectures[0], lectures[4], lectures[6]])
professorObj2 = Professor("Nataniel", "Garro", 322, [lectures[0], lectures[2], lectures[3]])

subjects = ""
for subject in professorObj1.get_professor_subjects():
  subjects += f"   {subject['lecture'].lecture_name_and_duration()}\n"
print(f"Professor: {professorObj1.full_name()} teaches the following subjects:\n{subjects}")

print(f"Adding: {lectures[2]['lecture'].lecture_name_and_duration()}\n")
professorObj1.add_subject(lectures[2])
subjects = ""
for subject in professorObj1.get_professor_subjects():
  subjects += f"   {subject['lecture'].lecture_name_and_duration()}\n"
print(f"Professor: {professorObj1.full_name()} teaches the following subjects:\n{subjects}")

print(f"Deleting: {lectures[0]['lecture'].lecture_name_and_duration()}")
professorObj1.remove_subject(lectures[0])
subjects = ""
for subject in professorObj1.get_professor_subjects():
  subjects += f"   {subject['lecture'].lecture_name_and_duration()}\n"
print(f"Student: {professorObj1.full_name()} attends the following subjects:\n{subjects}")