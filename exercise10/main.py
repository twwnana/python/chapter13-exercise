import requests

input_value = input("Enter gitlab username to list projects: ")

response = requests.get(f"https://gitlab.com/api/v4/users/{input_value}/projects")
my_projects = response.json()

for project in my_projects:
  print(f"Project name: {project['name']}")
  print(f"Url: {project['web_url']}\n")

