def youngestEmployee(employee_list):
  youngest_age = 0
  employeeObj = {}
  for employee in employee_list:
    if youngest_age == 0:
      youngest_age = employee["age"]
      employeeObj = employee
    elif employee["age"] < youngest_age:
      youngest_age = employee["age"]
      employeeObj = employee
      
  print("Youngest Employee: ") 
  print(f"Name: {employeeObj['name']}")
  print(f"Age: {employeeObj['age']}")
  