calculations = 0
continue_answer = ""
operationCounter = 0

def askQuestion():
  is_input_valid = False
  user_input1 = -1
  user_input2 = -1
  operator_input = "*"
  
  while is_input_valid == False:
    try:
      user_input1 = int(input("Enter value for num1: "))
      is_input_valid = True
    except:
      user_input1 = -1
      is_input_valid = False
      print(f"Invalid value entered for num1: {user_input1}")
  
  is_input_valid = False # reset input validity for num2 question
  while is_input_valid == False:
    try:
      user_input2 = int(input("Enter value for num2: "))
      is_input_valid = True
    except:
      user_input2 = -1
      is_input_valid = False
      print(f"Invalid value entered for num2: {user_input2}")
  
  is_operator_valid = False # reset input validity for operator
  while is_operator_valid == False:
    try:
      operator_input = input("Select operator to use: ")
      
      if operator_input != "*" and operator_input != "+" and operator_input != "/" and operator_input != "-":
        print(f"Invalid Operator selected: {operator_input}")
        raise Exception("operator invalid.")
      
      is_operator_valid = True
    except:
      operator_input = "*"
      is_operator_valid = False
      
  return {'num1': user_input1, 'num2': user_input2, 'operator': operator_input}

def performOperation(question_answer):
  num1 = question_answer['num1']
  num2 = question_answer['num2']
  operator = question_answer['operator']
  
  global operationCounter
  operationCounter += 1
  
  if operator == "+":
    return num1 + num2
  elif operator == "-":
    return num1 - num2
  elif operator == "*":
    return num1 * num2
  else:
    return num1 / num2
  
while continue_answer != "exit":
  question_answer = askQuestion()
  operation_answer = performOperation(question_answer)
  print(f"{question_answer['num1']} {question_answer['operator']} {question_answer['num2']} = {operation_answer}")
  
  continue_answer = input("Do you want another set of question? (type exit to quit): ").lower()
  
print(f"Operations conducted: {operationCounter}")