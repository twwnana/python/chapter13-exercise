import random

user_answer = -1
random.seed(None, 2)
correct_answer = random.randint(1, 9)

print("I have a number, Guess what the number is? ")

while user_answer != correct_answer:
  try:
    user_answer = int(input("Enter your guess!> "))
    
    if (user_answer > correct_answer):
      print("You guess too high!")
    elif (user_answer < correct_answer):
      print("You guess too low!")
    else:
      print("Correct!! YOU WON!!! Wuutwuut!!!")
    
  except:
    print("Invalid Number / Number Range.")
    user_answer = -1