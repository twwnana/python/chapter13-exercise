employee = {
  "name": "Tim",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer"
}

print(f"{employee}")
employee["job"] = "Software Engineer"
del employee["age"]

print("Key:Value list")
for property in employee:
  print(f"{property}:{employee[property]}")

  
dict_one = {'a': 100, 'b': 400} 
dict_two = {'x': 300, 'y': 200}

# Merges dictionaries
new_dict = {**dict_one, **dict_two}

sum = 0
minimum = 0
maximum = 0
for element in new_dict:
  sum += new_dict[element]
  
  if minimum == 0:
    minimum = new_dict[element]
  elif new_dict[element] < minimum:
    minimum = new_dict[element]
      
  if maximum == 0:
    maximum = new_dict[element]
  elif new_dict[element] > maximum:
    maximum = new_dict[element]
  
print(f"Sum is {sum}")
print(f"Max is {maximum}")
print(f"Min is {minimum}")