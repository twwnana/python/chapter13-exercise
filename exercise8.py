import datetime

bday_input = input("Please enter your birth date(MM/DD/YYYY): ")
converted = datetime.datetime.strptime(bday_input, "%m/%d/%Y")
today = datetime.datetime.today()

next_bday = datetime.datetime(today.year + 1, converted.month, converted.day, converted.hour, converted.minute, converted.second)
remaining = next_bday - today

print(f"Remaining time before next Bday: {remaining}")